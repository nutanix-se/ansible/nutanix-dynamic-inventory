# Nutanix dynamic inventory plugin for Ansible

This module will generate a dynamic inventory for use within your own Ansible playbooks using the Nutanix Prism Element or Prism Central API from a simple yaml configuration file. This module will greatly simplify the process of maintaining a static inventory of a Nutanix infrastructure.

## Using this repository

Simply download (clone) the repository and start modifying files according to your needs.

```
git clone https://gitlab.com/nutanix-se/ansible/nutanix-dynamic-inventory myAnsibleProject/
```

Ideally, you'll want to use [Git](https://git-scm.com/) to manage your Ansible configuration files. For that purpose simply [fork](https://help.github.com/articles/fork-a-repo/) this repository into your own Git repository before cloning and customizing it. Git will allow you to version and roll-back changes with ease.

## Using the `nutanix` dynamic_invetory module

Start with your `./ansible.cfg` file...
- Either add any settings that you need for your playbook to  `./ansible.cfg` that you wish to use into the one provided in this repository. Alternatively you could add the following setting to your existing `./ansible.cfg` file. Essentially you want to add the inventory_plugins variable which includes the path to the `inventory_plugins/` directory in your local playbook as this directory contains the `nutanix.py` plugin. If using your own `./ansible.cfg` you will also need to add the `nutanix` plugin to the list of enabled plugins.

```
[defaults]
inventory_plugins  = ./plugins/inventory_plugins

[inventory]
# List of enabled inventory plugins and the order in which they are used.
enable_plugins = host_list, script, auto, yaml, ini, toml, nutanix
```

- Next, create a yaml file to provide the connection details (and optionally authentication metadata) to your Nutanix infrastructure based on `inventories/example_basic.yml`. Place this file in a location within the playbook - you should treat this file as you would normally treat your static inventory file.
	- Two example configuration files have been included in this repo;
		- [`inventories/example_basic.yml`](inventories/example_basic.yml)
		- [`inventories/example_auth.yml`](inventories/example_auth.yml)

Once you have created your inventory, you may need to add authentication for groups/hosts. Use your newly minted inventory file to print the hierarchy to screen using this command `ansible-inventory -i inventories/my-nutanix-deployment.yml --graph`.

From this commands output either add groups (beginning with @ or hosts to the authentication section as in the `inventories/example_auth.yml` example file.

### Sample `ansible-inventory --graph` Output
```
$ ansible-inventory -i inventories/example.yml --graph
@all:
|--@prism:
|  |--@all_cluster_vips:
|  |  |--phx_spoc004_1_vip
|  |  |--phx_spoc014_1_vip
|  |  |--phx_spoc016_4_vip
|  |--@all_cvms:
|  |  |--phx_spoc004_1_1_cvm_27e92126-2816-4a41-814c-6f67cafffdb3
|  |  |--phx_spoc014_1_1_cvm_30458561-8de2-4085-9ba2-1f87c7103b1d
|  |  |--phx_spoc016_4_1_cvm_2f1ac498-51c9-4d3f-a7c3-da2f6193f542
|  |--@all_hypervisors:
|  |  |--phx_spoc004_1_1_27e92126-2816-4a41-814c-6f67cafffdb3
|  |  |--phx_spoc014_1_1_30458561-8de2-4085-9ba2-1f87c7103b1d
|  |  |--phx_spoc016_4_1_2f1ac498-51c9-4d3f-a7c3-da2f6193f542
|  |--@all_ipmis:
|  |  |--phx_spoc004_1_1_ipmi_27e92126-2816-4a41-814c-6f67cafffdb3
|  |  |--phx_spoc014_1_1_ipmi_30458561-8de2-4085-9ba2-1f87c7103b1d
|  |  |--phx_spoc016_4_1_ipmi_2f1ac498-51c9-4d3f-a7c3-da2f6193f542
|  |--@all_vms:
|  |  |--autodc_vm_c63570d3-8bf3-4446-951a-7816348d0612
|  |  |--autodc_vm_c8ddc875-7f18-480e-b66d-c786a35a80d0
|  |  |--centos_vm_7fa92c03-9cae-4826-a4f2-0ab59a4c1b12
|  |  |--centos_vm_fe0a43ef-f700-4ce1-9f63-bc3d4487ad85
|  |  |--hycu_vm_398aec72-8777-4f17-b54f-b6fd3fee1875
|  |  |--hycu_vm_47d7f831-f369-41d4-be88-9bf95b884eb2
|  |  |--move_vm_242dc234-7234-4efe-8c37-71cadae6465d
|  |  |--move_vm_f0a8b529-a4cd-4fc8-afef-3744b6a41ad3
|  |  |--oracle_db_vm_4cc7fac1-9184-41bf-9834-036899d9f044
|  |  |--oracle_db_vm_d7c914b6-ba42-43c7-a874-4a6492c9518b
|  |  |--rxautomationpc_4308a7c1-c944-4596-8e32-308b1134bd63
|  |  |--splunk_vm_1d2c6f0f-6c62-4047-82f4-d264b4eb66c8
|  |  |--splunk_vm_e9b8a02d-ee01-482f-b544-61a66944f04b
|  |  |--sql_server_2014_vm_750b9a8a-9883-45dd-9704-9c5122dd5d16
|  |  |--sql_server_2014_vm_8b7880b4-16cc-4d22-b5ac-d430820489c7
|  |  |--windows_10_vm_3efaaee1-1040-4014-a35f-06f532f992ef
|  |  |--windows_10_vm_db3afa26-a240-4eac-8f16-949c2b870276
|  |  |--windows_2012_vm_f055bbbb-e114-497e-b627-c1b6a49640b7
|  |  |--windows_2012_vm_fd80084a-e07d-4f10-8c5f-60b3b5a8e8df
|  |  |--x_ray_vm_b666a964-b55c-45d4-9e83-b2782ba2747a
|  |  |--x_ray_vm_c66fb1bb-d2d3-4382-bad5-42e479a2d05a
|  |--@categories:
|  |  |--@appfamily:
|  |  |--@apptier:
|  |  |  |--@apptier_default:
|  |  |--@apptype:
|  |  |  |--@apptype_apache_spark:
|  |  |  |--@apptype_default:
|  |  |  |  |--hycu_vm_398aec72-8777-4f17-b54f-b6fd3fee1875
|  |  |  |  |--hycu_vm_47d7f831-f369-41d4-be88-9bf95b884eb2
|  |  |  |--@apptype_employee_payroll:
|  |  |  |--@apptype_exchange:
|  |  |  |--@apptype_git_server:
|  |  |  |--@apptype_hadoop:
|  |  |  |--@apptype_inventory:
|  |  |  |--@apptype_kubernetes:
|  |  |  |--@apptype_microsoft_sql:
|  |  |  |--@apptype_oracle_db:
|  |  |--@calmapplication:
|  |  |--@calmdeployment:
|  |  |--@calmpackage:
|  |  |--@calmservice:
|  |  |--@environment:
|  |  |  |--@environment_dev:
|  |  |  |--@environment_production:
|  |  |  |--@environment_staging:
|  |  |  |--@environment_testing:
|  |  |--@ostype:
|  |  |--@quarantine:
|  |  |  |--@quarantine_default:
|  |  |  |--@quarantine_forensics:
|  |  |--@templatetype:
|  |  |  |--@templatetype_application:
|  |  |  |  |--oracle_db_vm_4cc7fac1-9184-41bf-9834-036899d9f044
|  |  |  |--@templatetype_vm:
|  |  |--@virtualnetworktype:
|  |  |  |--@virtualnetworktype_tenant:
|  |  |  |--@virtualnetworktype_test:
|  |--@clusters:
|  |  |--@phx_spoc004_1:
|  |  |  |--@phx_spoc004_1_blocks:
|  |  |  |  |--@block_18fm6f380034:
|  |  |  |  |  |--phx_spoc004_1_1_27e92126-2816-4a41-814c-6f67cafffdb3
|  |  |  |  |  |--phx_spoc004_1_1_cvm_27e92126-2816-4a41-814c-6f67cafffdb3
|  |  |  |  |  |--phx_spoc004_1_1_ipmi_27e92126-2816-4a41-814c-6f67cafffdb3
|  |  |  |--@phx_spoc004_1_cvm:
|  |  |  |  |--phx_spoc004_1_1_cvm_27e92126-2816-4a41-814c-6f67cafffdb3
|  |  |  |--@phx_spoc004_1_hypervisor:
|  |  |  |  |--phx_spoc004_1_1_27e92126-2816-4a41-814c-6f67cafffdb3
|  |  |  |--@phx_spoc004_1_ipmi:
|  |  |  |  |--phx_spoc004_1_1_ipmi_27e92126-2816-4a41-814c-6f67cafffdb3
|  |  |  |--@phx_spoc004_1_vms:
|  |  |  |  |--autodc_vm_c8ddc875-7f18-480e-b66d-c786a35a80d0
|  |  |  |  |--centos_vm_fe0a43ef-f700-4ce1-9f63-bc3d4487ad85
|  |  |  |  |--hycu_vm_398aec72-8777-4f17-b54f-b6fd3fee1875
|  |  |  |  |--move_vm_f0a8b529-a4cd-4fc8-afef-3744b6a41ad3
|  |  |  |  |--oracle_db_vm_4cc7fac1-9184-41bf-9834-036899d9f044
|  |  |  |  |--splunk_vm_1d2c6f0f-6c62-4047-82f4-d264b4eb66c8
|  |  |  |  |--sql_server_2014_vm_750b9a8a-9883-45dd-9704-9c5122dd5d16
|  |  |  |  |--windows_10_vm_db3afa26-a240-4eac-8f16-949c2b870276
|  |  |  |  |--windows_2012_vm_f055bbbb-e114-497e-b627-c1b6a49640b7
|  |  |  |  |--x_ray_vm_b666a964-b55c-45d4-9e83-b2782ba2747a
|  |  |  |--phx_spoc004_1_vip
|  |  |--@phx_spoc014_1:
|  |  |  |--@phx_spoc014_1_blocks:
|  |  |  |  |--@block_18fm6f380044:
|  |  |  |  |  |--phx_spoc014_1_1_30458561-8de2-4085-9ba2-1f87c7103b1d
|  |  |  |  |  |--phx_spoc014_1_1_cvm_30458561-8de2-4085-9ba2-1f87c7103b1d
|  |  |  |  |  |--phx_spoc014_1_1_ipmi_30458561-8de2-4085-9ba2-1f87c7103b1d
|  |  |  |--@phx_spoc014_1_cvm:
|  |  |  |  |--phx_spoc014_1_1_cvm_30458561-8de2-4085-9ba2-1f87c7103b1d
|  |  |  |--@phx_spoc014_1_hypervisor:
|  |  |  |  |--phx_spoc014_1_1_30458561-8de2-4085-9ba2-1f87c7103b1d
|  |  |  |--@phx_spoc014_1_ipmi:
|  |  |  |  |--phx_spoc014_1_1_ipmi_30458561-8de2-4085-9ba2-1f87c7103b1d
|  |  |  |--@phx_spoc014_1_vms:
|  |  |  |  |--rxautomationpc_4308a7c1-c944-4596-8e32-308b1134bd63
|  |  |  |--phx_spoc014_1_vip
|  |  |--@phx_spoc016_4:
|  |  |  |--@phx_spoc016_4_blocks:
|  |  |  |  |--@block_19fm6f090076:
|  |  |  |  |  |--phx_spoc016_4_1_2f1ac498-51c9-4d3f-a7c3-da2f6193f542
|  |  |  |  |  |--phx_spoc016_4_1_cvm_2f1ac498-51c9-4d3f-a7c3-da2f6193f542
|  |  |  |  |  |--phx_spoc016_4_1_ipmi_2f1ac498-51c9-4d3f-a7c3-da2f6193f542
|  |  |  |--@phx_spoc016_4_cvm:
|  |  |  |  |--phx_spoc016_4_1_cvm_2f1ac498-51c9-4d3f-a7c3-da2f6193f542
|  |  |  |--@phx_spoc016_4_hypervisor:
|  |  |  |  |--phx_spoc016_4_1_2f1ac498-51c9-4d3f-a7c3-da2f6193f542
|  |  |  |--@phx_spoc016_4_ipmi:
|  |  |  |  |--phx_spoc016_4_1_ipmi_2f1ac498-51c9-4d3f-a7c3-da2f6193f542
|  |  |  |--@phx_spoc016_4_vms:
|  |  |  |  |--autodc_vm_c63570d3-8bf3-4446-951a-7816348d0612
|  |  |  |  |--centos_vm_7fa92c03-9cae-4826-a4f2-0ab59a4c1b12
|  |  |  |  |--hycu_vm_47d7f831-f369-41d4-be88-9bf95b884eb2
|  |  |  |  |--move_vm_242dc234-7234-4efe-8c37-71cadae6465d
|  |  |  |  |--oracle_db_vm_d7c914b6-ba42-43c7-a874-4a6492c9518b
|  |  |  |  |--splunk_vm_e9b8a02d-ee01-482f-b544-61a66944f04b
|  |  |  |  |--sql_server_2014_vm_8b7880b4-16cc-4d22-b5ac-d430820489c7
|  |  |  |  |--windows_10_vm_3efaaee1-1040-4014-a35f-06f532f992ef
|  |  |  |  |--windows_2012_vm_fd80084a-e07d-4f10-8c5f-60b3b5a8e8df
|  |  |  |  |--x_ray_vm_c66fb1bb-d2d3-4382-bad5-42e479a2d05a
|  |  |  |--phx_spoc016_4_vip
|  |--@projects:
|  |  |--@default:
|  |  |  |--autodc_vm_c63570d3-8bf3-4446-951a-7816348d0612
|  |  |  |--autodc_vm_c8ddc875-7f18-480e-b66d-c786a35a80d0
|  |  |  |--centos_vm_7fa92c03-9cae-4826-a4f2-0ab59a4c1b12
|  |  |  |--centos_vm_fe0a43ef-f700-4ce1-9f63-bc3d4487ad85
|  |  |  |--hycu_vm_398aec72-8777-4f17-b54f-b6fd3fee1875
|  |  |  |--hycu_vm_47d7f831-f369-41d4-be88-9bf95b884eb2
|  |  |  |--move_vm_242dc234-7234-4efe-8c37-71cadae6465d
|  |  |  |--move_vm_f0a8b529-a4cd-4fc8-afef-3744b6a41ad3
|  |  |  |--oracle_db_vm_4cc7fac1-9184-41bf-9834-036899d9f044
|  |  |  |--oracle_db_vm_d7c914b6-ba42-43c7-a874-4a6492c9518b
|  |  |  |--rxautomationpc_4308a7c1-c944-4596-8e32-308b1134bd63
|  |  |  |--splunk_vm_1d2c6f0f-6c62-4047-82f4-d264b4eb66c8
|  |  |  |--splunk_vm_e9b8a02d-ee01-482f-b544-61a66944f04b
|  |  |  |--sql_server_2014_vm_750b9a8a-9883-45dd-9704-9c5122dd5d16
|  |  |  |--sql_server_2014_vm_8b7880b4-16cc-4d22-b5ac-d430820489c7
|  |  |  |--windows_10_vm_3efaaee1-1040-4014-a35f-06f532f992ef
|  |  |  |--windows_10_vm_db3afa26-a240-4eac-8f16-949c2b870276
|  |  |  |--windows_2012_vm_f055bbbb-e114-497e-b627-c1b6a49640b7
|  |  |  |--windows_2012_vm_fd80084a-e07d-4f10-8c5f-60b3b5a8e8df
|  |  |  |--x_ray_vm_b666a964-b55c-45d4-9e83-b2782ba2747a
|  |  |  |--x_ray_vm_c66fb1bb-d2d3-4382-bad5-42e479a2d05a
|  |--prism_vip
|--@ungrouped:
```

## Important Notes
- Ansible groups can only contain specific characters that can make `valid python variable names` resulting in groups that consist of uppercase and lowercase letters ( A-Z , a-z ), digits ( 0-9 ), and the underscore character ( _ ). All group and host names have these characters replaced by `_` whever they occur and the case set to lower for uniformity.
- VM names along with Hardware names are not necessarily unique either within a cluster or across multiple clusters managed by a Prism Central instance. To make these work around this limitation;
	- All hypervisor, cvm and ipmi Ansible hosts are stored in the inventory with the format `[node name]_(cvm/ipmi)_[node uuid]`
	- All virtual machine Ansible hosts are stored in the inventory with the format `[vm name]_[vm uuid]`

## Contributing
Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Authors
Ross Davies - Initial work - [www.rossdavies.info](www.rossdavies.info)
See also the list of [contributors](-/graphs/master) who participated in this project.

## Acknowledgments
Thanks have to go to Nutanix for their great developer website [nutanix.dev](https://www.nutanix.dev/) - without having an API driven product and public documentation this work would not have been possible. Also huge thanks to both [Mark Lavi](https://www.linkedin.com/in/marklavi) & [Chris Rasmussen](http://www.linkedin.com/in/chrisrasmussennz) for assisting navigating the API and putting up with my constant queries... Also thanks to the following friends & colleagues for their continued advice and support.
- [Shane Lindsay](https://www.linkedin.com/in/shane-lindsay-b2b38166)
- [Mark Andreopoulos](https://www.linkedin.com/in/mark-andreopoulos-663b6a6)
- Chandru Karuppannan
- [Jasnoor Gill](https://in.linkedin.com/in/jasnoor)

## Copyright and license
Copyright 2020 Ross Davies, released under the [MIT license](LICENSE)
